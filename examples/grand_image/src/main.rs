use clap::Parser;
use grand::{Random, MCG};
use std::fs::File;
use std::io::BufWriter;
use std::path::Path;

/// Program generates randomly (by using grand crate) a PNG image containing black and white pixels.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// The seed value used for the random number generator
    #[arg(short, long, default_value_t = 0)]
    seed: u32,
    /// Path where the image is written to
    #[arg(short, long)]
    path: String,
}
fn main() {
    let args = Args::parse();

    let path = Path::new(&args.path);
    let file = File::create(path).unwrap();
    let ref mut w = BufWriter::new(file);

    const WIDTH: u16 = 1024;
    const HEIGHT: u16 = 1024;
    const NUMBER_OF_PIXELS: usize = 131072;

    let mut encoder = png::Encoder::new(w, WIDTH.into(), HEIGHT.into());
    encoder.set_color(png::ColorType::Grayscale);
    encoder.set_depth(png::BitDepth::One);
    let mut writer = encoder.write_header().unwrap();

    let mut mcg = MCG::from_seed(args.seed);

    let mut data = [0; NUMBER_OF_PIXELS];

    for i in (0..data.len()).step_by(2) {
        let random_value = mcg.next();
        let low = (random_value & 0xFF) as u8;
        let high = (random_value >> 8) as u8;
        data[i] = low;
        data[i + 1] = high;
    }

    writer.write_image_data(&data).unwrap();
}
