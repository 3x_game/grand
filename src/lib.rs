//! Crate for generating random numbers for games implemented in restricted environments like fantasy consoles.
//!
//! ## Example
//!
//! ```rust
//! use grand::{Random, MCG};
//!
//! // create MCG generator from a seed (ideally the seed is random as well)
//! let mut mcg = MCG::from_seed(0);
//!
//! // get random numbers
//! let value = mcg.next();
//! assert_eq!(0x915f, value);
//! let value = mcg.next();
//! assert_eq!(51723, value);
//! ```
//!
//! ## Design guidelines
//!
//! * Focus is small code size, small state size, high performance and "acceptable" randomness
//! * Since this random number generator is only used for games in restricted environments the quality of the random number doesn't need to be high
//! * Main use case is a video game implemented for the [wasm4](https://wasm4.org) fantasy console
#![no_std]
#![warn(missing_docs)]

/// Trait defining the APIs a random number generator of this crate implements.
///
/// ## Design details
///
/// * Main reason is for this trait is splitting interface from implementation
pub trait Random {
    /// Get the next random number and updates the state of the random number generator.
    ///
    /// ## Design details
    ///
    /// * Currently the focus is on implementing multiplicative congruential generator (MCG) the random value is "only" [u16]
    ///    * Random values generated with MCGs due have defects in the lower bits of the state, therefore only the most significant bytes of the state are returned
    fn next(&mut self) -> u16;
}

/// Struct that implements a [multiplicative congruential generator (MCG)](https://en.wikipedia.org/wiki/Linear_congruential_generator#m_a_power_of_2,_c_=_0).
///
/// ## Design decisions
///
/// * Due to the wasm background, the state and operations are deliberately chosen to be [u32] at most
/// * MCGs are known for some defects, especially in the lower bits of the state, therefore only the most significant bytes are used
/// * Multiplier taken from: [Steele, Vigna - Computationally easy, spectrally good multipliers for congruential pseudorandom number generators p. 8, table 4.](https://arxiv.org/abs/2001.05304)
/// * Each seed is multplied by 2 and added with 0. In this way the state is not initialized with 0.
/// * Uses `wrapping_mul` and `wrapping_add` to get a "cheaper" version of operations modulo 2^32 without panicking
pub struct MCG {
    state: u32,
}

impl Random for MCG {
    fn next(&mut self) -> u16 {
        /*
          Multiplier taken from:
            Steele, Vigna -
              Computationally easy, spectrally good multipliers for congruential
              pseudorandom number generators
            p. 8, table 4.
            https://arxiv.org/abs/2001.05304
        */
        const MULTIPLIER: u32 = 0x915f77f5;
        self.state = self.state.wrapping_mul(MULTIPLIER);
        // only use the most significant 16 bits
        let value = (self.state >> 16) as u16;
        value
    }
}

impl MCG {
    /// Create a new Random number generator from the given seed.
    ///
    /// ## Design details
    ///
    /// * Due to the main target being wasm32 the seed is deliberately chosen to be [u32]
    pub const fn from_seed(seed: u32) -> Self {
        // seed from 0 creates a sequence containing solely of 0s
        // Adding plus 1 does enable to differentiate the seeds 0 and 1
        let state = seed.wrapping_mul(2).wrapping_add(1);
        MCG { state }
    }
}
