# Readme - grand

* Simple crate for random number generation for games
* Focus is restricted environments, e.g. fantasy consoles like [wasm4](https://wasm4.org/)
* Currently realizes a multiplicative congruential generator (MCG)
  * MCGs are ok-ish but have their issues
  * However they are fast and small (e.g. state)
  * And for games in restricted environments it should be good enough

## Visual test images

* The following images contains the bits generated with grand to create a black and white image of dimension 1024 x 1024 (idea from [Bo Allen - A Simple Visual Example](https://boallen.com/random-numbers.html))

* Created with seed 0

![Visual randomness test created with seed 0](./visual_randomness_test_image_seed_0.png "Visual randomness test created with seed 0")

* Created with seed 1234

![Visual randomness test created with seed 1234](./visual_randomness_test_image_seed_1234.png "Visual randomness test created with seed 1234")

* Both images do contain non-random looking artifacts
* This is suboptimal but should be ok for generating random in a game with restricted environment

## Code coverage

* Date 2022-10-15

```shell
Filename                                                            Regions    Missed Regions     Cover   Functions  Missed Functions  Executed       Lines      Missed Lines     Cover    Branches   Missed Branches     Cover
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/home/paw/01-ED/09-Programming/10-rust/3x_game/grand/src/lib.rs           7                 0   100.00%           5                 0   100.00%          46                 0   100.00%           0                 0         -
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TOTAL                                                                     7                 0   100.00%           5                 0   100.00%          46                 0   100.00%           0                 0         -
```

## Links

### Statistical Tests

* [Random.org - Analysis](https://www.random.org/analysis/)
* [Bo Allen - A Simple Visual Example](https://boallen.com/random-numbers.html)
* [Wikipedia - Die Hard Tests](https://en.wikipedia.org/wiki/Diehard_tests)
* [Wikipedia - Randomness Test](https://en.wikipedia.org/wiki/Randomness_test)
* [Diehard](https://web.archive.org/web/20160125103112/http://stat.fsu.edu/pub/diehard/)
* [Adam's  Blag - Random Numbers](https://www.arm64.ca/post/taocp-vol-2-seminumerical-algorithms/)
* [Linux Man - dieharder](https://linux.die.net/man/1/dieharder)
* [dieharder](https://webhome.phy.duke.edu/~rgb/General/dieharder.php)
